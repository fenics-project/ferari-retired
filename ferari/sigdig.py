# Copyright (C) 2006 Robert C. Kirby
#
# This file is part of FErari.
#
# FErari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FErari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FErari. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2005-04-01
# Last changed: 2006-04-01

import numpy

def round_sig( a , m ):
    if abs( a ) < 10**(-m):
        return 0.0
    else:
        m_str , e_str = ( "%.12e" % (a,) ).split( "e" )
        return round( float( m_str ) , m ) * 10**int(e_str)

def vec_round_sig( a , m ):
    return numpy.array( [ round_sig( x , m ) for x in a ] )

def tensor_round_sig( a , m ):
    asize = reduce( lambda a,b:a*b , a.shape )
    flat = numpy.reshape( a , (asize,1) )
    around = vec_round_sig( flat , m )
    return numpy.reshape( around , a.shape )

if __name__ == "__main__":
	a = 1.66666666666666666666
	print "%.16e" % (a,)
	print round_sig( a , 8 )
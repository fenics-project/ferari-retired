# Copyright (C) 2006 Robert C. Kirby
#
# This file is part of FErari.
#
# FErari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FErari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FErari. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2005-04-01
# Last changed: 2006-04-01

#from ffc.compiler.compiler import *
import sigdig

def symmetric_g( a ):
    if numpy.rank( a ) != 2 or a.shape[0] != a.shape[1]:
        print a
        raise RuntimeError, "Illegal matrix"
    n = a.shape[ 0 ]
    new_len = n * (n+1) / 2
    result = numpy.zeros( (new_len,) , "d" )
    cur = 0
    for i in range( n ):
        result[ cur ] = a[ i , i ]
        cur += 1
        for j in range( i+1 , n ):
            result[ cur ] = a[ i , j ] + a [ j , i ]
            cur += 1

    return result


def laplacianform( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a =  v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    return ac.reference_tensor()

################################################################
# The following functions take a shape and degree and return
# the reference tensor as a dictionary mapping indices to
# the appropriately processed tensors
################################################################
def laplacian( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a =  v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    A0 = sigdig.tensor_round_sig( ac.reference_tensor() , 10 )
    # put in symmetric part, symmetry transform applied
    Adict = {}
    for i in range(A0.shape[0]):
        for j in range(i,A0.shape[1]):
            Adict[i,j] = sigdig.vec_round_sig( symmetric_g( A0[i,j] ) , 10 )
    return Adict

def laplacian_matvec( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a =  v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    A0 = sigdig.tensor_round_sig( ac.reference_tensor() , 10 )
    Adict = {}
    for i in range(A0.shape[0]):
        Adict[i] = numpy.reshape( A0[i] , (-1,) )
    return Adict


def weighted_laplacian( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    w = Function( el )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a = w * v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    A0 = sigdig.tensor_round_sig( ac.reference_tensor() , 10 )
    Adict = {}
    for i in range(A0.shape[0]):
        for j in range(i,A0.shape[1]):
            tmp = numpy.array( map( symmetric_g , A0[i,j] ) )
            tmp = numpy.reshape( tmp , (-1,) )
            Adict[i,j] = sigdig.vec_round_sig( tmp , 10 )
    return Adict

def weighted_laplacian_g_first( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    w = Function( el )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a = w * v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    A0 = sigdig.tensor_round_sig( ac.reference_tensor() , 10 )
    Adict = {}
    for i in range(A0.shape[0]):
        for j in range(i,A0.shape[1]):
            for k in range(0,A0.shape[2]):
                Adict[i,j,k] = symmetric_g(A0[i,j,k])
    return Adict

def weighted_laplacian_coeff_first( shape , degree ):
    el = FiniteElement( "Lagrange" , shape , degree )
    k = Index()
    dx = Integral( "interior" )
    w = Function( el )
    v = BasisFunction( el )
    u = BasisFunction( el )
    a = w * v.dx( k ) * u.dx( k ) * dx
    ac = build( a , "fooform" , "raw" )
    A0 = sigdig.tensor_round_sig( ac.reference_tensor() , 10 )
    Adict = {}
    for i in range(A0.shape[0]):
        for j in range(i,A0.shape[1]):
            tmp = numpy.array( map( symmetric_g, A0[i,j] ) )
            for k in range(tmp.shape[1]):
                Adict[i,j,k] = tmp[:,k]
    return Adict

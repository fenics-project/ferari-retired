eps = 1.e-10
precision = 5

def nnz( u ):
    nnz_cur = 0
    for ui in u:
	if abs( ui ) > eps:
	    nnz_cur += 1
    return nnz_cur

def normalize_sign( u ):
    """Returns u or -u such that the first nonzero entry (up to eps)
    is positive"""
    for ui in u:
        if abs(ui) > eps:
            if ui < 0.0:
                return -u
            else:
                return u
    return u

def unit_vector( u ):
    mau = max(abs(u))
    if mau < eps:
        print u
	raise RuntimeError, "divide by zero"
    return normalize_sign( u / mau )

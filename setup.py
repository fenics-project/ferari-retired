#!/usr/bin/env python

from distutils.core import setup

setup(name="ferari", \
      version="1.0.0", \
      description="Optimizer for finite element code", \
      author="Robert C. Kirby", \
      author_email="kirby@uchicago.edu", \
      url="http://people.cs.uchicago.edu/~kirby", \
      license="LGPL v3 or later", \
      packages=['ferari'] )
